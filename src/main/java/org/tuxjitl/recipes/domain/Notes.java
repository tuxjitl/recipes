package org.tuxjitl.recipes.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(exclude = "recipe")
public class Notes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // PS: if note is deleted ==> recipe remains in db ==> no cascade here
    @OneToOne
    private Recipe recipe;//relation owned by recipe

    //This means that this annotated field should be represented
    // as a LOB in the database ==> no worries about size of the string
    @Lob
    private String recipeNotes;

}
