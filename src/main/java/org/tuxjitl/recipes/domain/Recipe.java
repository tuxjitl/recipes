package org.tuxjitl.recipes.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
//@EqualsAndHashCode(exclude = {"ingredients","notes","categories"})
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;
    private Integer prepTime;
    private Integer cookTime;
    private Integer servings;
    private String source;
    private String url;

    @Lob
    private String directions;

    //Why string instead of ordinal? When updating the enum,
    // the ordinal might change, the strings don't
    @Enumerated(value = EnumType.STRING)
    private Difficulty difficulty;

    // 1 recipe has zero or more ingredients
    //mappedby ==> recipe is the field in the child object ==> recipe
    //class Recipe is owner of the relationship
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "recipe")
    private Set<Ingredient> ingredients = new HashSet<>();

    //This means that this annotated field should be represented
    // as a BLOB in the database
    @Lob
    private Byte[] image;

    // PS: if recipe is deleted ==> note is also deleted ==> cascade here
    @OneToOne(cascade = CascadeType.ALL)//owner
    private Notes notes;

    @ManyToMany
    @JoinTable(name = "recipe_category",
        joinColumns = @JoinColumn(name = "recipe_id"),
        inverseJoinColumns = @JoinColumn(name = "category_id"))
    private Set<Category> categories = new HashSet<>();

    public void setNotes(Notes notes) {

        this.notes = notes;
        notes.setRecipe(this);
    }

   //bidirectionality ==> ingredient.setrecipe(this)
    public Recipe addIngredient(Ingredient ingredient){
        ingredient.setRecipe(this);
        this.ingredients.add(ingredient);
        return this;
    }

}
