package org.tuxjitl.recipes.repositories;

import org.springframework.data.repository.CrudRepository;
import org.tuxjitl.recipes.domain.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe,Long> {

}
