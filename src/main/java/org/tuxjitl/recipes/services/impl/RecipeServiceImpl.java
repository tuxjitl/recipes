package org.tuxjitl.recipes.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.tuxjitl.recipes.domain.Recipe;
import org.tuxjitl.recipes.repositories.RecipeRepository;
import org.tuxjitl.recipes.services.RecipeService;

import java.util.HashSet;
import java.util.Set;

@Service
@Slf4j
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;

    public RecipeServiceImpl(RecipeRepository recipeRepository) {

        this.recipeRepository = recipeRepository;
    }

    @Override
    public Set<Recipe> getRecipes() {

        //demo slf4j
        log.debug("I'm in the recipe service");

        //convert iterable to hashset
        Set<Recipe> recipeSet = new HashSet<>();
        recipeRepository.findAll().iterator().forEachRemaining(recipeSet::add);

        return recipeSet;
    }
}
