package org.tuxjitl.recipes.repositories;

import org.springframework.data.repository.CrudRepository;
import org.tuxjitl.recipes.domain.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient,Long> {

}
