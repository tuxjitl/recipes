package org.tuxjitl.recipes.services;

import org.tuxjitl.recipes.domain.Recipe;

import java.util.Set;

public interface RecipeService {
    Set<Recipe> getRecipes();

}
