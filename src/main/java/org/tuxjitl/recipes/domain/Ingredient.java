package org.tuxjitl.recipes.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@EqualsAndHashCode(exclude = "recipe")
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;
    private BigDecimal amount;

    //references just a reference table, so no cascade
    @OneToOne(fetch = FetchType.EAGER)//FetchType ==> default = Eager, just to demo
    private UnitOfMeasure uom;

    //to which recipe does the ingredient belong ==> relationship
    //no cascade here: delete an ingredient ==> recipe must still be in db
    //why no cascade annotation? default is none
    @ManyToOne
    private Recipe recipe;

    public Ingredient() {

    }

    public Ingredient(String description, BigDecimal amount, UnitOfMeasure uom) {

        this.description = description;
        this.amount = amount;
        this.uom = uom;
    }

    public Ingredient(String description, BigDecimal amount, UnitOfMeasure uom, Recipe recipe) {

        this.description = description;
        this.amount = amount;
        this.uom = uom;
        this.recipe=recipe;
    }


}
